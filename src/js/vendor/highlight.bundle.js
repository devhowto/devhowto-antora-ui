;(function doHighlight() {
  'use strict'

  var hljs = require('highlight.js/lib/highlight');

  hljs.registerLanguage('asciidoc', require('highlight.js/lib/languages/asciidoc'));
  hljs.registerLanguage('bash', require('highlight.js/lib/languages/bash'));
  hljs.registerLanguage('cpp', require('highlight.js/lib/languages/cpp'));
  hljs.registerLanguage('diff', require('highlight.js/lib/languages/diff'));
  hljs.registerLanguage('xml', require('highlight.js/lib/languages/xml'));
  hljs.registerLanguage('dockerfile', require('highlight.js/lib/languages/dockerfile'));
  hljs.registerLanguage('haskell', require('highlight.js/lib/languages/haskell'));
  hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));
  hljs.registerLanguage('typescript', require('highlight.js/lib/languages/typescript'));
  hljs.registerLanguage('css', require('highlight.js/lib/languages/css'));
  hljs.registerLanguage('json', require('highlight.js/lib/languages/json'));
  hljs.registerLanguage('lua', require('highlight.js/lib/languages/lua'));
  hljs.registerLanguage('nix', require('highlight.js/lib/languages/nix'));
  hljs.registerLanguage('none', require('highlight.js/lib/languages/plaintext'));
  hljs.registerLanguage('python', require('highlight.js/lib/languages/python'));
  hljs.registerLanguage('ruby', require('highlight.js/lib/languages/ruby'));
  hljs.registerLanguage('shell', require('highlight.js/lib/languages/shell'));
  hljs.registerLanguage('sql', require('highlight.js/lib/languages/sql'));
  hljs.registerLanguage('yaml', require('highlight.js/lib/languages/yaml'));

  ;[].slice.call(document.querySelectorAll('pre code.hljs[data-lang]')).forEach(function (node) {
    hljs.highlightBlock(node)
  });
})();
